package com.epam.starmarines.galaxy;

import com.google.common.collect.ImmutableList;
import org.jgrapht.Graph;
import org.jgrapht.Graphs;

import java.util.*;

/**
 * Обходчик графа волнами.
 */
public class WaveGraphVisitor<P> {

    private final Graph<P, ?> graph;
    private final Queue<P> visited = new LinkedList<>();
    private List<P> wave;

    public WaveGraphVisitor(Graph<P, ?> graph, P source) {
        this.graph = graph;
        visited.add(source);
        wave = Collections.singletonList(source);
    }

    public List<P> nextWave() {

        if (wave.isEmpty()) {
            return ImmutableList.of();
        }

        ImmutableList.Builder<P> builder = ImmutableList.builder();

        for (P vertex : wave) {
            for (P neighbour : Graphs.neighborListOf(graph, vertex)) {
                if (!visited.contains(neighbour)) {
                    visited.add(neighbour);
                    builder.add(neighbour);
                }
            }
        }

        wave = builder.build();
        return wave;
    }
}
