/*
 * © EPAM Systems, 2012  
 */
package com.epam.starmarines.galaxy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.ToIntFunction;
import java.util.stream.Collectors;

/**
 * Содержит информацию о планете
 */
public class Planet {

    private static final Logger log = LoggerFactory.getLogger(Planet.class);

	private String id;
	private String owner;
	private int units;
	private PlanetType type;
	private List<Planet> neighbours = new ArrayList<Planet>();
    private Map<Planet, Integer> estimatedArrival = new HashMap<>();
    private int estimatedEmigration = 0;

	/**
	 * Создать планету с заданным id
	 */
	public Planet(String id) {
		this.id = id;
	}

    public Planet(String id, String owner, int units, PlanetType type) {
        this.id = id;
        this.owner = owner;
        this.units = units;
        this.type = type;
    }

    /**
	 * Получить id планеты
	 */
	public String getId() {
		return id;
	}

	/**
	 * Получить владельца планеты
	 */
	public String getOwner() {
		return owner;
	}

	/**
	 * Задать владельца планеты
	 */
	public void setOwner(String owner) {
		this.owner = owner;
	}

	/**
	 * Получить количество юнитов на планете
	 */
	public int getUnits() {
		return units;
	}

	/**
	 * Задать количество юнитов на планете
	 */
	public void setUnits(int units) {
		this.units = units;
	}

	/**
	 * Получить тип планеты
	 */
	public PlanetType getType() {
		return type;
	}

	/**
	 * Задать тип планеты
	 */
	public void setType(PlanetType type) {
		this.type = type;
	}

	/**
	 * Получить список соседей планеты
	 */
	public List<Planet> getNeighbours() {
		return Collections.unmodifiableList(neighbours);
	}

	/**
	 * Добавить соседа
	 */
	public void addNeighbours(Planet neighbour) {
		neighbours.add(neighbour);
	}

    /**
     * Добавить units имигрантов с планеты from на данную.
     * @param from
     * @param units
     */
    public void addUnitsFrom(Planet from, int units) {
        // Планета источник должа быть рядом
        if (getNeighbours().contains(from)) {
            if (estimatedArrival.containsKey(from)) {
                estimatedArrival.put(from, estimatedArrival.get(from) + units);
            } else {
                estimatedArrival.put(from, units);
            }
        } else {
            log.error("Неверная команда Move: " + new Move(from, this, units).toString());
        }
    }

    /**
     * Увеличить число эмигрантов с этой планеты.
     * @param emigrants
     */
    public void increaseEmigrantUnits(int emigrants) {
        this.estimatedEmigration += emigrants;
    }

    /**
     * Получить ожидаемое количество юнитов на планете до стадии регенерации. Т.е. учитывается текущее количество юнитов
     * с учетом ожидаемой миграции ({@link #estimatedEmigration}  и {@link #estimatedArrival}).
     * @return
     */
    public int getEstimatedUnits() {

        Optional<Integer> immigrantSum = estimatedArrival.values()
                .stream()
                .reduce((t, u) -> t + u);

        return units + immigrantSum.orElse(0) - estimatedEmigration;
    }

	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		for (Planet neighbour : neighbours) {
			result.append('{').append(neighbour.getId()).append(", ").append(neighbour.getOwner())
                    .append(", ").append(neighbour.getUnits()).append('}');
		}

		return "Planet [id=" + id + ", owner=" + owner + ", Units=" + units + "/" + type.getLimit() + ", neighbours=" + result.toString() + "]\n";
	}

}
