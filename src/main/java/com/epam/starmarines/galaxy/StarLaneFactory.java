package com.epam.starmarines.galaxy;

import org.jgrapht.EdgeFactory;

/**
 * Created by Максим on 03.08.2014.
 */
public class StarLaneFactory<P> implements EdgeFactory<P, StarLane<P>> {

    @Override
    public StarLane<P> createEdge(P sourceVertex, P targetVertex) {
        return new StarLane<>(sourceVertex, targetVertex);
    }
}
