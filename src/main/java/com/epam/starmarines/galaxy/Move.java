/*
 * © EPAM Systems, 2012  
 */
package com.epam.starmarines.galaxy;

/**
 * Содержит информацию об одной команде на передвижение юнитов.
 */
public class Move {

    public static class Key {
        public final Planet from;
        public final Planet to;

        public Key(Planet from, Planet to) {
            this.from = from;
            this.to = to;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Key key = (Key) o;

            if (from != null ? !from.equals(key.from) : key.from != null) return false;
            if (to != null ? !to.equals(key.to) : key.to != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = from != null ? from.hashCode() : 0;
            result = 31 * result + (to != null ? to.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
            return "Key{" +
                    "from=" + from.getId() +
                    ", to=" + to.getId() +
                    '}';
        }
    }

	private final Planet from;
	private final Planet to;
	private final int amount;

	/*
	 * Создать новую команду
	 */
	public Move(Planet from, Planet to, int amount) {
		this.from = from;
		this.to = to;
		this.amount = amount;
	}

	/**
	 * Получить планету, с которой отправляются юниты
	 */
	public Planet getFrom() {
		return from;
	}

	/**
	 * Получить планету, на которую отправляются юниты
	 */
	public Planet getTo() {
		return to;
	}

	/**
	 * Получить количество отправляемых юнитов
	 */
	public int getAmount() {
		return amount;
	}

	@Override
	public String toString() {
		return "from " + from.getId() + " to " + to.getId() + " -- " + amount;
	}
}
