package com.epam.starmarines.galaxy;

/**
 * Created by Максим on 03.08.2014.
 */
public class StarLane<P> {

    private final P source;
    private final P target;

    public StarLane(P source, P target) {
        this.source = source;
        this.target = target;
    }

    public P getSource() {
        return source;
    }

    public P getTarget() {
        return target;
    }

    @Override
    public String toString() {
        return "StarLane{" +
                "source=" + source +
                ", target=" + target +
                '}';
    }
}
