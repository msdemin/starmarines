package com.epam.starmarines.galaxy;

import org.jgrapht.Graph;
import org.jgrapht.graph.SimpleGraph;

import java.util.Collection;

/**
 * Created by Максим on 03.08.2014.
 */
public class GalaxyFactory {


    public Graph<Planet, StarLane<Planet>> createGalaxy(Collection<Planet> planets) {

        Graph<Planet, StarLane<Planet>> galaxy = new SimpleGraph<>(new StarLaneFactory<>());

        planets.forEach(galaxy::addVertex);

        planets.forEach(planet -> {
            planet.getNeighbours().forEach(neighbour -> galaxy.addEdge(planet, neighbour));
        });

        return galaxy;
    }

}
