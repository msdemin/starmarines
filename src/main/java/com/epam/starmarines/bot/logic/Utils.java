package com.epam.starmarines.bot.logic;

import com.epam.starmarines.galaxy.Planet;
import com.epam.starmarines.galaxy.PlanetType;

/**
 * Created by Максим on 02.08.2014.
 */
public class Utils {

    public static int nextSize(Planet planet) {
        return Math.min(planet.getUnits() * (100 + planet.getType().getIncrement()) / 100,
                planet.getType().getLimit());
    }

    /**
     * Расчитать оптимальное количество юнитов на планете. Это такое количество, при котором на следующий ход
     * регенерация на планете заполнит ее предел на 100%.
     * @return
     */
    public static int optimalUnitsCount(Planet planet) {
        return planet.getType().getLimit() * 100 / (100 + planet.getType().getIncrement());

    }

    public static int maxRegen(Planet p) {
        int result = p.getUnits() * p.getType().getIncrement() / 100;
        System.out.println(p.getId() + ", maxRegen: " + result);
        return result;
    }


    /**
     * Максимальная реальная регенерация на планете.
     * @param planet планета
     * @return Максимальная регенерация на планете
     */
    public static int realRegen(Planet planet) {
        int units = planet.getUnits();
        PlanetType planetType = planet.getType();
        int min = Math.min(units * planetType.getIncrement() / 100, planetType.getLimit() - units);
        int result = min > 0 ? min : 0;
        System.out.println(planet.getId() + ", realRegen: " + result);
        return result;
    }

    /**
     * Избыток юнитов на планете с учетом миграций.
     * @return
     */
    public static int excess(Planet planet) {
        return Math.max(
                planet.getEstimatedUnits() - Utils.optimalUnitsCount(planet),
                0);
    }

}
