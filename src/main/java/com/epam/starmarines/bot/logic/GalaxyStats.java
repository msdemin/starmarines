package com.epam.starmarines.bot.logic;

import com.epam.starmarines.galaxy.Planet;
import com.epam.starmarines.galaxy.StarLane;
import org.jgrapht.Graph;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by Максим on 13.08.2014.
 */
public class GalaxyStats {

    public Map<String, Empire> empires(Collection<Planet> planets) {

        Map<String, Integer> units = planets.stream()
                .collect(
                        Collectors.groupingBy(
                                Planet::getOwner,
                                Collectors.reducing(0, Planet::getUnits, Integer::sum)
                        )
                );

        Map<String, Empire> empires = new HashMap<>();
        for (Map.Entry<String, Integer> statsEntry : units.entrySet()) {
            empires.put(statsEntry.getKey(), new Empire(statsEntry.getKey(), statsEntry.getValue()));
        }

        return empires;
    }
}
