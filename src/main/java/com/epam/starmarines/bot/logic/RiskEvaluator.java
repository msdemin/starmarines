package com.epam.starmarines.bot.logic;

import com.epam.starmarines.bot.logic.PlanetInvasionRisk;
import com.epam.starmarines.galaxy.Planet;
import com.epam.starmarines.galaxy.StarLane;
import org.jgrapht.Graph;

import java.util.List;

/**
 * Created by Максим on 16.08.2014.
 */
public interface RiskEvaluator {
    List<PlanetInvasionRisk> evaluate(Graph<Planet, StarLane<Planet>> galaxy, Planet planet);
}
