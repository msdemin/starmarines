package com.epam.starmarines.bot.logic;

import com.epam.starmarines.galaxy.Planet;
import com.epam.starmarines.galaxy.StarLane;
import org.jgrapht.Graph;

/**
 * Created by Максим on 20.08.2014.
 */
public class Planets {
    public static Planet findById(Graph<Planet, StarLane<Planet>> galaxy, String planetId) {
        return galaxy.vertexSet().stream().filter(p -> p.getId().equals(planetId)).findFirst().get();
    }
}
