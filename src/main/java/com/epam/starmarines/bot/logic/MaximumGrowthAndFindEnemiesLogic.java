/*
 * © EPAM Systems, 2012  
 */
package com.epam.starmarines.bot.logic;

import com.epam.starmarines.galaxy.*;
import org.jgrapht.Graph;
import org.jgrapht.alg.DijkstraShortestPath;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Логика максимального роста.
 */
public class MaximumGrowthAndFindEnemiesLogic implements Logic {

    private static final Logger log = LoggerFactory.getLogger(MaximumGrowthAndFindEnemiesLogic.class);

    public static final Comparator<Planet> PLANET_TYPE_COMPARATOR = new Comparator<Planet>() {
        @Override
        public int compare(Planet o1, Planet o2) {
            return o2.getType().getIncrement() - o1.getType().getIncrement();
        }
    };
    private final String botName;
    private final GalaxyFactory galaxyFactory = new GalaxyFactory();
    private Predicate<Planet> myPlanetPredicate;

    private String baseId = null;
    private String c0Id = null;
    private String c1Id = null;
    private String c2Id = null;
    private String b1Id = null;
    private String b2Id = null;
    private String aId = null;

    public MaximumGrowthAndFindEnemiesLogic(String botName) {
        this.botName = botName;
        myPlanetPredicate = p -> p.getOwner().equals(this.botName);
    }

    /**
     * Основной метод бота, который на основе полученных данных решает какие команды необходимо отправить на сервер
     *
     * @param planets текущее состояние планет
     * @return команды на передвижение юнитов
     */
    @Override
    public Collection<Move> step(Collection<Planet> planets) {
        // просто вывести на консоль полученные от сервера данные
        for (Planet planet : planets) {
            System.out.print(planet);
        }

        if (baseId == null) {
            Planet base = planets.stream()
                    .filter(myPlanetPredicate)
                    .filter(p -> p.getType().equals(PlanetType.TYPE_D))
                    .findFirst().get();
            baseId = base.getId();
            Planet planetC0 = base.getNeighbours().get(0);
            List<Planet> planetsC = planetC0.getNeighbours().stream().filter(p -> p.getType().equals(PlanetType.TYPE_C)).collect(Collectors.toList());
            c0Id = planetC0.getId();
            c1Id = planetsC.get(0).getId();
            c2Id = planetsC.get(1).getId();
            b1Id = planetC0.getNeighbours().stream()
                    .filter(p -> p.getType().equals(PlanetType.TYPE_B))
                    .filter(p -> p.getNeighbours().stream().anyMatch(n -> n.getId().equals(c1Id)))
                    .findFirst().get().getId();
            b2Id = planetC0.getNeighbours().stream()
                    .filter(p -> p.getType().equals(PlanetType.TYPE_B))
                    .filter(p -> p.getNeighbours().stream().anyMatch(n -> n.getId().equals(c2Id)))
                    .findFirst().get().getId();
            aId = planets.stream().filter(p -> p.getType().equals(PlanetType.TYPE_A)).findFirst().get().getId();
            log.info("Initial configuration: baseId={}, c0Id={}, c1Id={}, c2Id={}, b1Id={}, b2Id={}", baseId, c0Id, c1Id, c2Id, b1Id, b2Id);
        }

        Graph<Planet, StarLane<Planet>> galaxy = galaxyFactory.createGalaxy(planets);


        // список для хранения комманд
        Collection<Move> moves = new ArrayList<Move>();

        java.util.function.Predicate<Planet> byMyName = p -> p.getOwner().equals(botName);
        Comparator<Planet> byIncrement = (p1, p2) -> p1.getType().getIncrement() - p2.getType().getIncrement();

        List<Planet> myPlanets = planets.stream()
                .filter(byMyName)
                .sorted(byIncrement)
                .collect(Collectors.toCollection(ArrayList::new));

        for (Planet planet : myPlanets.stream().filter(p -> !p.getType().equals(PlanetType.TYPE_A)).collect(Collectors.toList())) {
            takeSuperiorFreePlanet(galaxy, moves, byIncrement, planet);
        }


        Map<String, Empire> empires = new GalaxyStats().empires(galaxy.vertexSet());
        if (empires.get(botName) != null && empires.get(botName).getUnits() > 2100) {
            return throughWeakCenter(moves, galaxy);
        } else {
            if (empires.get(botName) != null)
            plazdarm(moves, galaxy);
        }

        // restore b1 and b2
        MoveHelper helper = new MoveHelper(galaxy, moves);
//        if (galaxy.vertexSet().stream().filter(p -> p.getId().equals(baseId)).findFirst().isPresent()) {
//            Planet base = Planets.findById(galaxy, baseId);
//            Planet c0 = Planets.findById(galaxy, c0Id);
//            Planet c1 = Planets.findById(galaxy, c1Id);
//            Planet c2 = Planets.findById(galaxy, c2Id);
//            Planet b1 = Planets.findById(galaxy, b1Id);
//            Planet b2 = Planets.findById(galaxy, b2Id);
//            Planet a = Planets.findById(galaxy, aId);
//
//            boolean c1IsMine = c1.getOwner().equals(botName);
//            boolean c2IsMine = c2.getOwner().equals(botName);
//            boolean b1IsMine = b1.getOwner().equals(botName);
//            boolean b2IsMine = b2.getOwner().equals(botName);
//            boolean aIsMine = a.getOwner().equals(botName);
//            if (aIsMine && (!b1IsMine || !b2IsMine)) {
//                if (!b1IsMine) {
//                    helper.createMove(a, b1, Math.min(a.getUnits() - 1, 201));
//                } else if (!b2IsMine) {
//                    helper.createMove(a, b2, Math.min(a.getUnits() - 1, 201));
//                }
//            }
//        }



        earlyStrategyForStrongBase(galaxy, moves, myPlanets);

        for (int iteration = 0; iteration < 5; iteration++) {
            for (Planet planet : myPlanets) {
                WaveGraphVisitor<Planet> waveGraphVisitor = new WaveGraphVisitor<>(galaxy, planet);
                List<Planet> candidateTargets = waveGraphVisitor.nextWave();

                // Определяем количество войск для переноса
                int optimalSize = Utils.optimalUnitsCount(planet);
                int availableUnits = planet.getEstimatedUnits() - optimalSize;

                while (availableUnits > 0 && !candidateTargets.isEmpty()) {

                    // Перенос войск для подпитки союзных планет
                    if (availableUnits > 0) {
                        List<Planet> myNeighbourPlanets = candidateTargets.stream()
                                .filter(p -> p.getOwner().equals(botName))
                                .sorted(PLANET_TYPE_COMPARATOR)
                                .collect(Collectors.toList());
                        for (Planet myPlanet : myNeighbourPlanets) {
                            int wantUnits = Math.max(0, Utils.optimalUnitsCount(myPlanet) - Utils.nextSize(myPlanet));
                            if (wantUnits > 0) {
                                int sendUnitsCount = Math.min(wantUnits, availableUnits);
                                availableUnits -= sendUnitsCount;
                                createMove(galaxy, moves, planet, myPlanet, sendUnitsCount);
                            }
                        }
                    }

                    // Атака врагов и занятие свободных планет
                    if (availableUnits > 0) {
                        List<Planet> bestEnemyPlanets = candidateTargets.stream()
                                .filter(p -> !p.getOwner().equals(botName))
                                .sorted(PLANET_TYPE_COMPARATOR)
                                .collect(Collectors.toList());

                        if (!bestEnemyPlanets.isEmpty()) {
                            createMove(galaxy, moves, planet, bestEnemyPlanets.get(0), availableUnits);
                            availableUnits = 0;
                        }
                    }

                    candidateTargets = waveGraphVisitor.nextWave();
                }

                if (availableUnits > 0) {
                    log.error("Не распределено юнитов [" + availableUnits + "] с планеты " + planet);
                }
            }
        }


//        final Map<Move.Key, Integer> reducedMoves = new HashMap<>();
//        for (final Move move : moves) {
//            Move.Key key = new Move.Key(move.getFrom(), move.getTo());
//            if (reducedMoves.containsKey(key)) {
//                reducedMoves.put(key, reducedMoves.get(key) + move.getAmount());
//            } else {
//                reducedMoves.put(key, move.getAmount());
//            }
//        }
//
//        moves = reducedMoves.entrySet().stream()
//                .map(entry -> new Move(entry.getKey().from, entry.getKey().to, entry.getValue()))
//                .collect(Collectors.toList());

        // выводим команды на консоль перед отправкой
        for (Move move : moves) {
            System.out.println(move);
        }

        return moves;
    }

    private void plazdarm(Collection<Move> moves, Graph<Planet, StarLane<Planet>> galaxy) {
        // Сначала занимаем планеты c1Id и c2Id
        MoveHelper helper = new MoveHelper(galaxy, moves);

        log.info("plazdarm!!!!!!!!!!!!!!!!!!!!!!!");

        Planet base = Planets.findById(galaxy, baseId);
        Planet c0 = Planets.findById(galaxy, c0Id);
        Planet c1 = Planets.findById(galaxy, c1Id);
        Planet c2 = Planets.findById(galaxy, c2Id);
        Planet b1 = Planets.findById(galaxy, b1Id);
        Planet b2 = Planets.findById(galaxy, b2Id);

        boolean c1IsMine = c1.getOwner().equals(botName);
        boolean c2IsMine = c2.getOwner().equals(botName);
        boolean b1IsMine = b1.getOwner().equals(botName);
        boolean b2IsMine = b2.getOwner().equals(botName);

        fromC0toPeripheralC(helper, c0, c1);
        fromC0toPeripheralC(helper, c0, c2);

        helper.createMove(base, c0, Utils.excess(base));
        if (c1IsMine && c2IsMine
                && c1.getEstimatedUnits() > 335
                && c2.getEstimatedUnits() > 335) {
            if (!b1IsMine) {
                helper.createMove(c0, b1, Utils.excess(c0));
            }
            if (!b2IsMine) {
                helper.createMove(c0, b2, Utils.excess(c0));
            }
        }

        if (c1IsMine && !c2IsMine && !b1IsMine) {
            helper.createMove(c1, b1, Utils.excess(c1));
            helper.createMove(c0, b1, Utils.excess(c0));
        }
        if (c2IsMine && !c1IsMine && !b2IsMine) {
            helper.createMove(c2, b2, Utils.excess(c2));
            helper.createMove(c0, b2, Utils.excess(c0));
        }


        if (Utils.excess(c0) > 0) {
            if (!c1IsMine && (c0.getUnits() + b1.getUnits()) > 600) {
                helper.createMove(b1, c1, b1.getUnits() - 1);
                helper.createMove(c0, c1, c0.getUnits() - 1);
                c0.setUnits(1);
            } else if (!c2IsMine && (c0.getUnits() + b2.getUnits()) > 600) {
                helper.createMove(b2, c2, b2.getUnits() - 1);
                helper.createMove(c0, c2, c0.getUnits() - 1);
                c0.setUnits(1);
            }
        }
    }

    private Collection<Move> throughWeakCenter(Collection<Move> moves, Graph<Planet, StarLane<Planet>> galaxy) {


        // Сначала занимаем планеты c1Id и c2Id
        MoveHelper helper = new MoveHelper(galaxy, moves);

        Planet base = Planets.findById(galaxy, baseId);
        Planet c0 = Planets.findById(galaxy, c0Id);
        Planet c1 = Planets.findById(galaxy, c1Id);
        Planet c2 = Planets.findById(galaxy, c2Id);
        Planet b1 = Planets.findById(galaxy, b1Id);
        Planet b2 = Planets.findById(galaxy, b2Id);

        boolean c1IsMine = c1.getOwner().equals(botName);
        boolean c2IsMine = c2.getOwner().equals(botName);
        boolean b1IsMine = b1.getOwner().equals(botName);
        boolean b2IsMine = b2.getOwner().equals(botName);

//        helper.createMove(base, c0, Utils.excess(base));
//        for (int i = 0; i < 3; i++) {
//            if (c1IsMine && c2IsMine
//                    && c1.getEstimatedUnits() > 335
//                    && c2.getEstimatedUnits() > 335) {
//                if (!b1IsMine) {
//                    helper.createMove(c0, b1, Utils.excess(c0));
//                }
//                if (!b2IsMine) {
//                    helper.createMove(c0, b2, Utils.excess(c0));
//                }
//            }
//
//            if (c1IsMine && !c2IsMine && !b1IsMine) {
//                helper.createMove(c1, b1, Utils.excess(c1));
//                helper.createMove(c0, b1, Utils.excess(c0));
//            }
//            if (c2IsMine && !c1IsMine && !b2IsMine) {
//                helper.createMove(c2, b2, Utils.excess(c2));
//                helper.createMove(c0, b2, Utils.excess(c0));
//            }
//
//            fromC0toPeripheralC(helper, c0, c1);
//            fromC0toPeripheralC(helper, c0, c2);
//
//            if (c0.getUnits() > 350) {
//                if (!c1IsMine && (c0.getUnits() + b1.getUnits()) > 600) {
//                    helper.createMove(b1, c1, b1.getUnits() - 1);
//                    helper.createMove(c0, c1, c0.getUnits() - 1);
//                    c0.setUnits(1);
//                } else if (!c2IsMine && (c0.getUnits() + b2.getUnits()) > 600) {
//                    helper.createMove(b2, c2, b2.getUnits() - 1);
//                    helper.createMove(c0, c2, c0.getUnits() - 1);
//                    c0.setUnits(1);
//                }
//            }
//        }


        // Сначала восстановим максимальный рост
        moves.addAll(refillPlanets(galaxy));

        Planet center = galaxy.vertexSet().stream().filter(p -> p.getType().equals(PlanetType.TYPE_A)).findFirst().get();

        for (int i = 0; i < 5; i++) {
            WaveGraphVisitor<Planet> centerWaves = new WaveGraphVisitor<>(galaxy, center);
            List<Planet> planets = centerWaves.nextWave();
            while (!planets.isEmpty()) {
                List<Planet> myPlanets = planets.stream()
                        .filter(myPlanetPredicate)
                        .collect(Collectors.toList());
                for (Planet myPlanet : myPlanets) {
                    int availableUnits = Utils.excess(myPlanet);
                    if (availableUnits < 500) { // Если это не ударный кулак, то отправляем войска в центр
                        createMove(galaxy, moves, myPlanet, center, availableUnits);
                    }
                }
                planets = centerWaves.nextWave();
            }
        }


        Map<String, Empire> empires = new GalaxyStats().empires(galaxy.vertexSet());

        if (center.getUnits() > 2000) {
            List<Planet> targets;
            if (!c1IsMine) {
                targets = Collections.singletonList(c1);
            } else if (!c2IsMine) {
                targets = Collections.singletonList(c2);
            } else {
                targets = galaxy.vertexSet().stream()
                        .filter(myPlanetPredicate.negate())
                        .sorted(PLANET_TYPE_COMPARATOR
                                .thenComparing(
                                        (p1, p2) ->
                                                DijkstraShortestPath.findPathBetween(galaxy, base, p1).size() -
                                                        DijkstraShortestPath.findPathBetween(galaxy, base, p2).size()))
                                //.thenComparing((p1, p2) -> empires.get(p2.getOwner()).getUnits() - empires.get(p1.getOwner()).getUnits() ))
                        .collect(Collectors.toList());
            }
            if (!targets.isEmpty()) {
                int sendUnitsCount = center.getUnits() - 1;
                log.info("Launching MegaFist from {} to {}: {}", center.getId(), targets.get(0).getId(), sendUnitsCount);
                createMove(galaxy, moves, center, targets.get(0), sendUnitsCount);
            }
        }

        List<Planet> fists = galaxy.vertexSet().stream()
                .filter(myPlanetPredicate)
                .filter(p -> Utils.excess(p) > 300)
                .filter(p -> !p.getType().equals(PlanetType.TYPE_A))
                .collect(Collectors.toList());
        for (final Planet fist : fists) {
            Optional<Planet> first;
//            if (!c1IsMine) {
//                first = Optional.of(c1);
//            } else if (!c2IsMine) {
//                first = Optional.of(c2);
//            } else {
                first = galaxy.vertexSet().stream()
                        .filter(myPlanetPredicate.negate())
                        .sorted(PLANET_TYPE_COMPARATOR
                                        .thenComparing((p1, p2) ->
                                                        DijkstraShortestPath.findPathBetween(galaxy, fist, p1).size() -
                                                                DijkstraShortestPath.findPathBetween(galaxy, fist, p2).size()
                                        )
                        ).findFirst();
//            }
            int sendUnitsCount = fist.getUnits() - Utils.optimalUnitsCount(fist);
            if (first.isPresent()) {
                log.info("Move MegaFist from {} to {}: {}", fist.getId(), first.get().getId(), sendUnitsCount);
            }
            if (first.isPresent()) {
                createMove(galaxy, moves, fist, first.get(), sendUnitsCount);
            }
        }

        return moves;
    }

    private boolean fromC0toPeripheralC(MoveHelper helper, Planet c0Planet, Planet peripheralC) {
        boolean c1IsMine = peripheralC.getOwner().equals(botName);
        if (c1IsMine) {
            int c1WantUnits = Math.max(0, Utils.optimalUnitsCount(peripheralC) - peripheralC.getUnits());
            if (c1WantUnits > 0 && Utils.excess(c0Planet) > 0) {
                int unitsToC1 = Math.min(c1WantUnits, c0Planet.getUnits() - 1);
                c0Planet.setUnits(c0Planet.getUnits() - unitsToC1);
                helper.createMove(c0Planet, peripheralC, unitsToC1);
            }
        }
        return c1IsMine;
    }

    private Collection<Move> refillPlanets(Graph<Planet, StarLane<Planet>> galaxy) {
        Collection<Move> moves = new ArrayList<>();

        List<Planet> planetsInNeed = galaxy.vertexSet().stream()
                .filter(myPlanetPredicate)
                .filter(p -> Utils.optimalUnitsCount(p) - p.getEstimatedUnits() > 0)
                .sorted(PLANET_TYPE_COMPARATOR)
                .collect(Collectors.toList());

        for (int i = 0; i < 10; i++) {
            for (Planet target : planetsInNeed) {
                List<Planet> donors = target.getNeighbours().stream()
                        .filter(myPlanetPredicate)
                        .sorted(PLANET_TYPE_COMPARATOR)
                        .collect(Collectors.toList());
                for (Planet donor : donors) {
                    int wantUnits = Utils.optimalUnitsCount(target) - target.getEstimatedUnits();
                    if (wantUnits > 0) {
                        createMove(galaxy, moves, donor, target, Utils.excess(donor));
                    }
                }
            }
        }


        return moves;
    }

    private void earlyStrategyForStrongBase(Graph<Planet, StarLane<Planet>> galaxy, Collection<Move> moves, List<Planet> myPlanets) {
        for (Planet planet : myPlanets) {
            // занятие свободных планет, если текущая планета уже почти достигла оптимального размера

            if (myPlanets.size() == 1 && planet.getType().equals(PlanetType.TYPE_D)) {
                int preOptimalSize = Utils.optimalUnitsCount(planet) * 100 / (100 + planet.getType().getIncrement());
                if (planet.getEstimatedUnits() >
                        preOptimalSize) {
                    WaveGraphVisitor<Planet> waveGraphVisitor = new WaveGraphVisitor<>(galaxy, planet);
                    List<Planet> candidateTargets = waveGraphVisitor.nextWave().stream()
                            .filter(p -> p.getOwner().length() == 0)
                            .sorted(PLANET_TYPE_COMPARATOR)
                            .collect(Collectors.toList());

                    if (!candidateTargets.isEmpty()) {
                        createMove(galaxy, moves, planet, candidateTargets.get(0), 3);
                    }
                }
            }

            boolean hasPlanetD = myPlanets.stream().filter(p -> p.getType().equals(PlanetType.TYPE_D)).count() > 0;
            if (hasPlanetD && myPlanets.size() == 2
                    && planet.getType().equals(PlanetType.TYPE_C)) {

                List<Planet> candidateTargets = planet.getNeighbours().stream()
                        .filter(p -> p.getOwner().length() == 0)
                        .filter(p -> p.getType().equals(PlanetType.TYPE_C))
                        .collect(Collectors.toList());

                for (Planet candidateTarget : candidateTargets) {
                    createMove(galaxy, moves, planet, candidateTarget, 1);
                }
            }
        }
    }

    private boolean takeSuperiorFreePlanet(Graph<Planet, StarLane<Planet>> galaxy, Collection<Move> moves, Comparator<Planet> byIncrement, Planet planet) {
        WaveGraphVisitor<Planet> waveGraphVisitor = new WaveGraphVisitor<>(galaxy, planet);
        List<Planet> candidateTargets = waveGraphVisitor.nextWave();

        // Если рядом есть свободная планета более высокого класса, то перенести туда все войска
        List<Planet> freeBetterNeighbourPlanets = candidateTargets.stream()
                .filter(p -> p.getOwner().equals("") && p.getType().getIncrement() > planet.getType().getIncrement())
                .sorted(byIncrement)
                .collect(Collectors.toList());
        if (!freeBetterNeighbourPlanets.isEmpty()) {
            // TODO: необходимо определять риски * вес качества планеты
            Planet target = freeBetterNeighbourPlanets.get(0);
            int amount = planet.getUnits();
            createMove(galaxy, moves, planet, target, amount);
//            moves.add(new Move(planet, target, amount));
//            target.addUnitsFrom(planet, amount);
            return true;
        }


//            // Если рядом есть свободная планета более высокого класса, то перенести туда все войска
//        List<Planet> freeBetterNeighbourPlanets = candidateTargets.stream()
//                .filter(p -> p.getOwner().equals("") && p.getType().getIncrement() > planet.getType().getIncrement())
//                .sorted(byIncrement)
//                .collect(Collectors.toList());
//        if (planet.getType().equals(PlanetType.TYPE_A)) {
//            if (!freeBetterNeighbourPlanets.isEmpty()) {
//                createMove(galaxy, moves, planet, freeBetterNeighbourPlanets.get(0), planet.getUnits());
//            }
//        } else {
//            for (Planet target : freeBetterNeighbourPlanets) {
//                createMove(galaxy, moves, planet, target, planet.getUnits() / 4);
//            }
//        }


//        if (!freeBetterNeighbourPlanets.isEmpty()) {
//            // TODO: необходимо определять риски * вес качества планеты
//            Planet target = freeBetterNeighbourPlanets.get(0);
//            int amount = planet.getUnits() * (new Random().nextInt(5) + 3) / 10;
//            if (planet.getType().equals(PlanetType.TYPE_A)) {
//                amount = planet.getUnits();
//            }
////            amount = planet.getUnits();
//            createMove(galaxy, moves, planet, target, amount);
////            moves.add(new Move(planet, target, amount));
////            target.addUnitsFrom(planet, amount);
//            return true;
//        }
        return false;
    }

    private boolean createMove(Graph<Planet, StarLane<Planet>> galaxy, Collection<Move> moves, Planet fromPlanet, Planet toPlanet, int sendUnitsCount) {
        List<StarLane<Planet>> path = DijkstraShortestPath.findPathBetween(galaxy, fromPlanet, toPlanet);
        if (!path.isEmpty()) {
            Planet source = path.get(0).getSource();
            Planet target = path.get(0).getTarget();
            Planet destination = source.equals(fromPlanet) ? target : source;
            int chunk = sendUnitsCount / 5;
            for (int i = 0; i < 4; i++) {
                moves.add(new Move(fromPlanet, destination, chunk));
            }
            Move move = new Move(fromPlanet, destination, sendUnitsCount - 4 * chunk);
            moves.add(move);
            destination.addUnitsFrom(fromPlanet, sendUnitsCount);
            fromPlanet.increaseEmigrantUnits(sendUnitsCount);
            return true;
        } else {
            log.error("Не удалось найти путь от " + fromPlanet.getId() + " до " + toPlanet.getId());
        }
        return false;
    }

}
