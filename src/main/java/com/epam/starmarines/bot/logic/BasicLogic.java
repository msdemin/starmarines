/*
 * © EPAM Systems, 2012  
 */
package com.epam.starmarines.bot.logic;

import java.util.*;

import com.epam.starmarines.galaxy.Move;
import com.epam.starmarines.galaxy.Planet;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Ordering;

/**
 * Содержит в себе логику бота. Из основной программы каждый ход вызывается метод {@link #step}
 */
public class BasicLogic implements Logic {

    public static final Comparator<Planet> PLANET_TYPE_COMPARATOR = new Comparator<Planet>() {
        @Override
        public int compare(Planet o1, Planet o2) {
            return o1.getType().compareTo(o2.getType());
        }
    };
    private final String botName;

    public BasicLogic(String botName) {
        this.botName = botName;
    }

    /**
     * Основной метод бота, который на основе полученных данных решает какие команды необходимо отправить на сервер
     *
     * @param galaxy текущее состояние планет
     * @return команды на передвижение юнитов
     */
    @Override
    public Collection<Move> step(Collection<Planet> galaxy) {
        // просто вывести на консоль полученные от сервера данные
        for (Planet planet : galaxy) {
            System.out.print(planet);
        }

        // список для хранения комманд
        Collection<Move> moves = new ArrayList<Move>();

        // тут находится вся логика
        for (Planet planet : galaxy) {
            if (botName.equals(planet.getOwner())) {

                final int planetLimit = planet.getType().getLimit();

                Predicate<Planet> freePlanetPredicate = new Predicate<Planet>() {
                    @Override
                    public boolean apply(Planet input) {
                        return input.getOwner().equals("");
                    }
                };
                Predicate<Planet> minePlanetPredicate = new Predicate<Planet>() {
                    @Override
                    public boolean apply(Planet input) {
                        return input.getOwner().equals(botName) && input.getType().getLimit() > planetLimit;
                    }
                };
                Predicate<Planet> enemyPlanetPredicate = new Predicate<Planet>() {
                    @Override
                    public boolean apply(Planet input) {
                        return !input.getOwner().equals(botName) && !input.getOwner().equals("");
                    }
                };

                List<Planet> bestFreePlanets = Ordering.from(PLANET_TYPE_COMPARATOR)
                        .greatestOf(Iterables.filter(planet.getNeighbours(), freePlanetPredicate), 1);

                List<Planet> bestMinePlanets = Ordering.from(PLANET_TYPE_COMPARATOR)
                        .greatestOf(Iterables.filter(planet.getNeighbours(), minePlanetPredicate), 1);

                List<Planet> bestEnemyPlanets = Ordering.from(PLANET_TYPE_COMPARATOR).reverse()
                        .greatestOf(Iterables.filter(planet.getNeighbours(), enemyPlanetPredicate), 1);

                int maxRegen = Utils.maxRegen(planet);
                int realRegen = Utils.realRegen(planet);
                int unitsToSend = maxRegen - realRegen;


                if (!bestFreePlanets.isEmpty()) {
                    Planet target = bestFreePlanets.get(0);
                    moves.add(new Move(planet, target, Math.max(unitsToSend, realRegen)));
                } else if (!bestMinePlanets.isEmpty() && bestMinePlanets.get(0).getUnits() < planet.getUnits()) {
                    boolean myBestTargetIsBigger = planet.getType().getLimit() < bestMinePlanets.get(0).getType().getLimit();
                    moves.add(new Move(planet, bestMinePlanets.get(0), myBestTargetIsBigger ? planet.getUnits() : unitsToSend));
                }
                else
                if (!bestEnemyPlanets.isEmpty()) {
                    Planet target = bestEnemyPlanets.get(0);
                    moves.add(new Move(planet, target, planet.getUnits()));
                }


//                moves.add(new Move(planet, planet.getNeighbours().get(0), planet.getUnits()));
//                break;
            }
        }

        // выводим команды на консоль перед отправкой
        for (Move move : moves) {
            System.out.println(move);
        }

        return moves;
    }

}
