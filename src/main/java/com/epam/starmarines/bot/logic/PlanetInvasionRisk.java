package com.epam.starmarines.bot.logic;

/**
 * Created by Максим on 16.08.2014.
 */
public class PlanetInvasionRisk {

    private final String bot;
    private final int amount;

    public PlanetInvasionRisk(String bot, int amount) {
        this.bot = bot;
        this.amount = amount;
    }

    public String getBot() {
        return bot;
    }

    public int getAmount() {
        return amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PlanetInvasionRisk that = (PlanetInvasionRisk) o;

        if (amount != that.amount) return false;
        if (bot != null ? !bot.equals(that.bot) : that.bot != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = bot != null ? bot.hashCode() : 0;
        result = 31 * result + amount;
        return result;
    }

    @Override
    public String toString() {
        return "PlanetInvasionRisk{" +
                "bot='" + bot + '\'' +
                ", amount=" + amount +
                '}';
    }
}
