/*
 * © EPAM Systems, 2012  
 */
package com.epam.starmarines.bot.logic.strongcenter;

import com.epam.starmarines.bot.logic.*;
import com.epam.starmarines.galaxy.*;
import org.jgrapht.Graph;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Логика максимального роста.
 */
public class StrongCenterLogic implements Logic {

    private static final Logger log = LoggerFactory.getLogger(StrongCenterLogic.class);

    public static final Comparator<Planet> PLANET_TYPE_COMPARATOR = new Comparator<Planet>() {
        @Override
        public int compare(Planet o1, Planet o2) {
            return o2.getType().getIncrement() - o1.getType().getIncrement();
        }
    };
    private final String botName;
    private final GalaxyFactory galaxyFactory = new GalaxyFactory();

    private int stepNumber = 0;
    private boolean kamikazeMode = true;

    public StrongCenterLogic(String botName) {
        this.botName = botName;
    }

    /**
     * Основной метод бота, который на основе полученных данных решает какие команды необходимо отправить на сервер
     *
     * @param planets текущее состояние планет
     * @return команды на передвижение юнитов
     */
    @Override
    public Collection<Move> step(Collection<Planet> planets) {

        stepNumber++;

        // просто вывести на консоль полученные от сервера данные
        for (Planet planet : planets) {
            System.out.print(planet);
        }

        Graph<Planet, StarLane<Planet>> galaxy = galaxyFactory.createGalaxy(planets);
        // список для хранения комманд
        Collection<Move> moves = new ArrayList<Move>();

        MoveHelper moveHelper = new MoveHelper(galaxy, moves);




        java.util.function.Predicate<Planet> byMyName = p -> p.getOwner().equals(botName);
        Comparator<Planet> byIncrement = (p1, p2) -> p1.getType().getIncrement() - p2.getType().getIncrement();

        List<Planet> myPlanets = planets.stream()
                .filter(byMyName)
                .sorted(byIncrement)
                .collect(Collectors.toCollection(ArrayList::new));

        for (Planet planet : myPlanets) {
            takeSuperiorFreePlanet(galaxy, byIncrement, planet, moveHelper);
        }

        earlyStrategyForStrongBase(galaxy, myPlanets, moveHelper);

        for (int iteration = 0; iteration < 5; iteration++) {
            for (Planet planet : myPlanets) {
                WaveGraphVisitor<Planet> waveGraphVisitor = new WaveGraphVisitor<>(galaxy, planet);
                List<Planet> candidateTargets = waveGraphVisitor.nextWave();

                // Определяем количество войск для переноса
                int optimalSize = Utils.optimalUnitsCount(planet);
                int availableUnits = planet.getEstimatedUnits() - optimalSize;

                while (availableUnits > 0 && !candidateTargets.isEmpty()) {

                    // Перенос войск для подпитки союзных планет
                    if (availableUnits > 0) {
                        List<Planet> myNeighbourPlanets = candidateTargets.stream()
                                .filter(p -> p.getOwner().equals(botName))
                                .sorted(PLANET_TYPE_COMPARATOR)
                                .collect(Collectors.toList());
                        for (Planet myPlanet : myNeighbourPlanets) {
                            int wantUnits = Math.max(0, Utils.optimalUnitsCount(myPlanet) - Utils.nextSize(myPlanet));
                            if (wantUnits > 0) {
                                int sendUnitsCount = Math.min(wantUnits, availableUnits);
                                availableUnits -= sendUnitsCount;
                                moveHelper.createMove(planet, myPlanet, sendUnitsCount);
                            }
                        }
                    }

                    // Атака врагов и занятие свободных планет
                    if (availableUnits > 0) {
                        List<Planet> bestEnemyPlanets = candidateTargets.stream()
                                .filter(p -> !p.getOwner().equals(botName))
                                .sorted(PLANET_TYPE_COMPARATOR)
                                .collect(Collectors.toList());

                        if (!bestEnemyPlanets.isEmpty()) {
                            moveHelper.createMove(planet, bestEnemyPlanets.get(0), availableUnits);
                            availableUnits = 0;
                        }
                    }

                    candidateTargets = waveGraphVisitor.nextWave();
                }

                if (availableUnits > 0) {
                    log.error("Не распределено юнитов [" + availableUnits + "] с планеты " + planet);
                }
            }
        }


        // выводим команды на консоль перед отправкой
        for (Move move : moves) {
            System.out.println(move);
        }

        return moves;
    }

    private void earlyStrategyForStrongBase(Graph<Planet, StarLane<Planet>> galaxy, List<Planet> myPlanets, MoveHelper moveHelper) {

        for (Planet planet : myPlanets) {
            // занятие свободных планет, если текущая планета уже почти достигла оптимального размера

            if (myPlanets.size() == 1 && planet.getType().equals(PlanetType.TYPE_D)) {
                int preOptimalSize = Utils.optimalUnitsCount(planet) * 100 / (100 + planet.getType().getIncrement());
                if (planet.getEstimatedUnits() >
                        preOptimalSize) {
                    WaveGraphVisitor<Planet> waveGraphVisitor = new WaveGraphVisitor<>(galaxy, planet);
                    List<Planet> candidateTargets = waveGraphVisitor.nextWave().stream()
                            .filter(p -> p.getOwner().length() == 0)
                            .sorted(PLANET_TYPE_COMPARATOR)
                            .collect(Collectors.toList());

                    if (!candidateTargets.isEmpty()) {
                        moveHelper.createMove(planet, candidateTargets.get(0), 3);
                    }
                }
            }

            boolean hasPlanetD = myPlanets.stream().filter(p -> p.getType().equals(PlanetType.TYPE_D)).count() > 0;
            if (hasPlanetD && myPlanets.size() == 2
                    && planet.getType().equals(PlanetType.TYPE_C)) {

                List<Planet> candidateTargets = planet.getNeighbours().stream()
                        .filter(p -> p.getOwner().length() == 0)
                        .filter(p -> p.getType().equals(PlanetType.TYPE_C))
                        .collect(Collectors.toList());

                for (Planet candidateTarget : candidateTargets) {
                    moveHelper.createMove(planet, candidateTarget, 1);
                }
            }
        }
    }

    private boolean takeSuperiorFreePlanet(final Graph<Planet, StarLane<Planet>> galaxy,
                                           Comparator<Planet> byIncrement,
                                           Planet planet,
                                           MoveHelper moveHelper) {

        // Если центральная планета не моя, то бросаем туда всю мощь
        if (kamikazeMode && stepNumber > 3) {
            Optional<Planet> planetOptional = galaxy.vertexSet().stream()
                    .filter(p -> p.getType().equals(PlanetType.TYPE_D))
                    .findFirst();
            if (planetOptional.isPresent()) {
                Planet centerPlanet = planetOptional.get();
                if (!centerPlanet.getOwner().equals(botName)) {
                    List<Planet> myPlanetsTypeC = galaxy.vertexSet().stream()
                            .filter(p -> p.getOwner().equals(botName))
                            .collect(Collectors.toList());
                    for (Planet myPlanet : myPlanetsTypeC) {
                        moveHelper.createMove(myPlanet, centerPlanet, myPlanet.getUnits());
                    }
                }
            }
        }


        WaveGraphVisitor<Planet> waveGraphVisitor = new WaveGraphVisitor<>(galaxy, planet);
        List<Planet> candidateTargets = waveGraphVisitor.nextWave();

        final RiskEvaluator riskEvaluator = new SimpleRiskEvaluator();

        GalaxyStats galaxyStats = new GalaxyStats();
        Map<String, Empire> empires = galaxyStats.empires(galaxy.vertexSet());

        if (empires.size() <= 3 && false) { // осталось два участника и нейтральные планеты
            List<Planet> freeBetterNeighbourPlanets = candidateTargets.stream()
                    .filter(p -> !p.getOwner().equals(botName) && p.getType().getIncrement() > planet.getType().getIncrement())
                    .sorted(byIncrement)
                    .collect(Collectors.toList());
            if (!freeBetterNeighbourPlanets.isEmpty()) {
                moveHelper.createMove(planet, freeBetterNeighbourPlanets.get(0), planet.getUnits());
                return true;
            }
        }


        if (empires.get(botName).getUnits() < 700) {
            // Если рядом есть планета более высокого класса, то перенести туда все войска, если риск оправдан
            // Иначе заслать туда разведчиков (1 юнит)
            List<Planet> freeBetterNeighbourPlanets = candidateTargets.stream()
                    .filter(p -> //p.getOwner().equals("") &&
                            p.getType().getIncrement() > planet.getType().getIncrement())
                    .sorted(byIncrement)
                    .sorted(new PlanetByEnemyRiskComparator(botName, galaxy, riskEvaluator).reversed())
                    .collect(Collectors.toList());
            for (Planet target : freeBetterNeighbourPlanets) {
                List<PlanetInvasionRisk> risks = riskEvaluator.evaluate(galaxy, target);
                log.info("Risks for {}: {}", target, risks);
                if (risks.size() == 1) { // В соседях у планеты только я
                    moveHelper.createMove(planet, target, planet.getUnits());
                } else {
                    PlanetInvasionRisk maxForce = risks.get(0);
                    int scoutAmount = target.getType().equals(PlanetType.TYPE_D)
                            || planet.getEstimatedUnits() < 3
                            || !target.getOwner().equals("") ? 1 : 1;
                    if (maxForce.getBot().equals(botName)) {
                        int unitsLeftAfterBattle = maxForce.getAmount() - risks.get(1).getAmount();
                        if (unitsLeftAfterBattle > 40) {
                            moveHelper.createMove(planet, target, planet.getEstimatedUnits());
                        } else {
                            moveHelper.createMove(planet, target, scoutAmount);
                        }
                    } else {
                        moveHelper.createMove(planet, target, scoutAmount);
                    }
                }
            }
        }


//        if (planet.getType().equals(PlanetType.TYPE_A)) {
//            if (!freeBetterNeighbourPlanets.isEmpty()) {
//                moveHelper.createMove(planet, freeBetterNeighbourPlanets.get(0), planet.getUnits());
//            }
//        } else {
//
//        }
        return false;
    }


}
