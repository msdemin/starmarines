package com.epam.starmarines.bot.logic;

import com.epam.starmarines.bot.logic.PlanetInvasionRisk;
import com.epam.starmarines.bot.logic.RiskEvaluator;
import com.epam.starmarines.galaxy.Planet;
import com.epam.starmarines.galaxy.StarLane;
import org.jgrapht.Graph;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by Максим on 16.08.2014.
 */
public class SimpleRiskEvaluator implements RiskEvaluator {

    @Override
    public List<PlanetInvasionRisk> evaluate(Graph<Planet, StarLane<Planet>> galaxy, Planet planet) {

        List<Planet> neighbours = planet.getNeighbours();
        List<Planet> all = new ArrayList<>();
        if (planet.getOwner().length() > 0) {
            all.add(planet);
        }
        all.addAll(neighbours);

        Map<String, Integer> unitsSum = all.stream()
                .collect(Collectors.groupingBy(
                        Planet::getOwner,
                        Collectors.reducing(
                                0,
                                Planet::getUnits,
                                Integer::sum
                        )
                ));

        // Для владельца планеты применяем рост
        if (planet.getOwner().length() > 0) {
            Integer ownerRisk = unitsSum.get(planet.getOwner());
            Planet ownerPlanetOnNextTurn = new Planet(planet.getId(), planet.getOwner(), ownerRisk, planet.getType());
            unitsSum.put(planet.getOwner(), Utils.nextSize(ownerPlanetOnNextTurn));
        }

        List<PlanetInvasionRisk> result = unitsSum.entrySet().stream()
                .filter(entry -> entry.getKey().length() > 0)
                .map(entry -> new PlanetInvasionRisk(entry.getKey(), entry.getValue()))
                .sorted((o1, o2) -> o2.getAmount() - o1.getAmount())
                .collect(Collectors.toList());

        return result;
    }


}
