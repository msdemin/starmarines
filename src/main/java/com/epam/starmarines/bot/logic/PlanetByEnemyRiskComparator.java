package com.epam.starmarines.bot.logic;

import com.epam.starmarines.galaxy.Planet;
import com.epam.starmarines.galaxy.StarLane;
import org.jgrapht.Graph;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

/**
 * Created by Максим on 17.08.2014.
 */
public class PlanetByEnemyRiskComparator implements Comparator<Planet> {

    final String myBotName;
    final Graph<Planet, StarLane<Planet>> galaxy;
    final RiskEvaluator riskEvaluator;

    public PlanetByEnemyRiskComparator(String myBotName, Graph<Planet, StarLane<Planet>> galaxy, RiskEvaluator riskEvaluator) {
        this.myBotName = myBotName;
        this.galaxy = galaxy;
        this.riskEvaluator = riskEvaluator;
    }

    @Override
    public int compare(Planet o1, Planet o2) {
        List<PlanetInvasionRisk> risks1 = riskEvaluator.evaluate(galaxy, o1);
        List<PlanetInvasionRisk> risks2 = riskEvaluator.evaluate(galaxy, o2);

        Optional<PlanetInvasionRisk> maxEnemy1 = risks1.stream().filter(r -> !r.getBot().equals(myBotName)).findFirst();
        Optional<PlanetInvasionRisk> maxEnemy2 = risks2.stream().filter(r -> !r.getBot().equals(myBotName)).findFirst();
        if (maxEnemy1.isPresent()) {
            if (maxEnemy2.isPresent()) {
                return maxEnemy1.get().getAmount() - maxEnemy2.get().getAmount();
            } else {
                return 1;
            }
        } else {
            return maxEnemy2.isPresent() ? -1 : 0;
        }
    }
}
