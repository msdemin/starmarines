/*
 * © EPAM Systems, 2012  
 */
package com.epam.starmarines.bot.logic;

import com.epam.starmarines.galaxy.Move;
import com.epam.starmarines.galaxy.Planet;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Ordering;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

/**
 * Логика максимального роста.
 */
public class MaximumGrowthLogic implements Logic {

    public static final Comparator<Planet> PLANET_TYPE_COMPARATOR = new Comparator<Planet>() {
        @Override
        public int compare(Planet o1, Planet o2) {
            return o1.getType().compareTo(o2.getType());
        }
    };
    private final String botName;

    public MaximumGrowthLogic(String botName) {
        this.botName = botName;
    }

    /**
     * Основной метод бота, который на основе полученных данных решает какие команды необходимо отправить на сервер
     *
     * @param galaxy текущее состояние планет
     * @return команды на передвижение юнитов
     */
    @Override
    public Collection<Move> step(Collection<Planet> galaxy) {
        // просто вывести на консоль полученные от сервера данные
        for (Planet planet : galaxy) {
            System.out.print(planet);
        }

        // список для хранения комманд
        Collection<Move> moves = new ArrayList<Move>();

        // тут находится вся логика
        for (final Planet planet : galaxy) {
            if (botName.equals(planet.getOwner())) {


                // Если рядом есть свободная планета более высокого класса, то перенести туда все войска
                Predicate<Planet> freePlanetPredicate = new Predicate<Planet>() {
                    @Override
                    public boolean apply(Planet input) {
                        return input.getOwner().equals("") && input.getType().getIncrement() > planet.getType().getIncrement();
                    }
                };
                List<Planet> bestFreePlanets = Ordering.from(PLANET_TYPE_COMPARATOR)
                        .greatestOf(Iterables.filter(planet.getNeighbours(), freePlanetPredicate), planet.getNeighbours().size());
                if (!bestFreePlanets.isEmpty()) {
                    Planet target = bestFreePlanets.get(0);
                    moves.add(new Move(planet, target, planet.getUnits()));
                    break;
                }

                // Определяем количество войск для переноса
                int optimalSize = Utils.optimalUnitsCount(planet);
                int availableUnits = planet.getUnits() - optimalSize;

                // Перенос войск для подпитки союзных планет
                Predicate<Planet> myPlanetPredicate = new Predicate<Planet>() {
                    @Override
                    public boolean apply(Planet input) {
                        return input.getOwner().equals(botName);
                    }
                };
                if (availableUnits > 0) {
                    List<Planet> myPlanets = Ordering.from(PLANET_TYPE_COMPARATOR)
                            .greatestOf(Iterables.filter(planet.getNeighbours(), myPlanetPredicate), planet.getNeighbours().size());
                    for (Planet myPlanet : myPlanets) {
                        int wantUnits = Math.max(0, Utils.optimalUnitsCount(myPlanet) - Utils.nextSize(myPlanet));
                        if (wantUnits > 0) {
                            int sendUnitsCount = Math.min(wantUnits, availableUnits);
                            availableUnits -= sendUnitsCount;
                            moves.add(new Move(planet, myPlanet, sendUnitsCount));
                        }
                        if (availableUnits == 0) {
                            break;
                        }
                    }
                }

                // Атака врагов и занятие свободных планет
                if (availableUnits > 0) {
                    Predicate<Planet> enemyOrFreePlanetPredicate = new Predicate<Planet>() {
                        @Override
                        public boolean apply(Planet input) {
                            return !input.getOwner().equals(botName);
                        }
                    };
                    List<Planet> bestEnemyPlanets = Ordering.from(PLANET_TYPE_COMPARATOR).reverse()
                            .greatestOf(Iterables.filter(planet.getNeighbours(), enemyOrFreePlanetPredicate), planet.getNeighbours().size());


                    if (!bestEnemyPlanets.isEmpty()) {
                        moves.add(new Move(planet, bestEnemyPlanets.get(0), availableUnits));
                        availableUnits = 0;
                    }

                }

                // Остатки перенаправляем союзным планетам
                if (availableUnits > 0) {
                    List<Planet> myPlanets = Ordering.from(PLANET_TYPE_COMPARATOR)
                            .greatestOf(Iterables.filter(planet.getNeighbours(), myPlanetPredicate), planet.getNeighbours().size());
                    if (!myPlanets.isEmpty()) {
                        int amount = availableUnits / myPlanets.size();
                        for (Planet myPlanet : myPlanets) {
                            moves.add(new Move(planet, myPlanet, amount));
                        }
                        availableUnits = 0;
                    }
                }
            }
        }

        // выводим команды на консоль перед отправкой
        for (Move move : moves) {
            System.out.println(move);
        }

        return moves;
    }

}
