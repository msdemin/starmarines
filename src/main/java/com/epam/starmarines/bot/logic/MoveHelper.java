package com.epam.starmarines.bot.logic;

import com.epam.starmarines.galaxy.Move;
import com.epam.starmarines.galaxy.Planet;
import com.epam.starmarines.galaxy.StarLane;
import org.jgrapht.Graph;
import org.jgrapht.alg.DijkstraShortestPath;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * Created by Максим on 16.08.2014.
 */
public class MoveHelper {

    private static final Logger log = LoggerFactory.getLogger(MoveHelper.class);

    private final Graph<Planet, StarLane<Planet>> galaxy;
    private final Collection<Move> moves;

    public MoveHelper(Graph<Planet, StarLane<Planet>> galaxy, Collection<Move> moves) {
        this.galaxy = galaxy;
        this.moves = moves;
    }

    public boolean createMove(Planet fromPlanet, Planet toPlanet, int sendUnitsCount) {
        List<StarLane<Planet>> path = DijkstraShortestPath.findPathBetween(galaxy, fromPlanet, toPlanet);
        if (!path.isEmpty()) {
            Planet source = path.get(0).getSource();
            Planet target = path.get(0).getTarget();
            Planet destination = source.equals(fromPlanet) ? target : source;
            destination.addUnitsFrom(fromPlanet, sendUnitsCount);
            fromPlanet.increaseEmigrantUnits(sendUnitsCount);
            int chunk = sendUnitsCount / 5;
            for (int i = 0; i < 4; i++) {
                moves.add(new Move(fromPlanet, destination, chunk));
            }
            Move move = new Move(fromPlanet, destination, sendUnitsCount - 4 * chunk);
            moves.add(move);
            return true;
        } else {
            log.error("Не удалось найти путь от " + fromPlanet.getId() + " до " + toPlanet.getId());
            return false;
        }
    }
}
