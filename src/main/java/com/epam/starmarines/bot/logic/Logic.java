package com.epam.starmarines.bot.logic;

import com.epam.starmarines.galaxy.Move;
import com.epam.starmarines.galaxy.Planet;

import java.util.Collection;

/**
 * Created by Максим on 02.08.2014.
 */
public interface Logic {
    Collection<Move> step(Collection<Planet> galaxy);
}
