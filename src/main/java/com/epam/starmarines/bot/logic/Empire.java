package com.epam.starmarines.bot.logic;

/**
 * Created by Максим on 13.08.2014.
 */
public class Empire {

    private String name;

    private int units;

    public Empire(String name, int units) {
        this.name = name;
        this.units = units;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getUnits() {
        return units;
    }

    public void setUnits(int units) {
        this.units = units;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Empire empire = (Empire) o;

        if (units != empire.units) return false;
        if (!name.equals(empire.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + units;
        return result;
    }

    @Override
    public String toString() {
        return "Empire{" +
                "name='" + name + '\'' +
                ", units=" + units +
                '}';
    }
}
