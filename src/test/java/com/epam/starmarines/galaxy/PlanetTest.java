package com.epam.starmarines.galaxy;

import com.google.common.collect.ImmutableList;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class PlanetTest {

    @Test
    public void testGetEstimatedUnits() throws Exception {
        int emigrantsFromP2toP1 = 30;

        Planet p2 = new Planet("2");
        p2.setUnits(500);
        p2.increaseEmigrantUnits(emigrantsFromP2toP1);

        Planet p = new Planet("1");
        p.setUnits(200);
        p.increaseEmigrantUnits(100);
        p.addNeighbours(p2);
        p.addUnitsFrom(p2, emigrantsFromP2toP1);

        int estimatedUnits = p.getEstimatedUnits();

        assert estimatedUnits == 200 + emigrantsFromP2toP1 - 100;

    }
}