package com.epam.starmarines.galaxy;

import com.epam.starmarines.bot.Response;
import org.jgrapht.Graph;
import org.testng.annotations.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class GalaxyFactoryTest {



    @Test
    public void testCreateGalaxy() throws Exception {

        //   (p1) ---- (p2) ---- (p3)
        //      \                /
        //       \-----(p4)-----/

        Planet p1 = planet("1");
        Planet p2 = planet("2");
        Planet p3 = planet("3");
        Planet p4 = planet("4");

        p1.addNeighbours(p2);
        p1.addNeighbours(p4);

        p2.addNeighbours(p1);
        p2.addNeighbours(p3);

        p3.addNeighbours(p2);
        p3.addNeighbours(p4);

        p4.addNeighbours(p1);
        p4.addNeighbours(p3);

        Response resp = new Response();
        resp.addPlanet(p1);
        resp.addPlanet(p2);
        resp.addPlanet(p3);
        resp.addPlanet(p4);

        GalaxyFactory f = new GalaxyFactory();

        Graph<Planet, StarLane<Planet>> galaxy = f.createGalaxy(resp.getPlanets());

        assertThat(galaxy.vertexSet().size(), equalTo(4));

        assertThat(galaxy.edgeSet().size(), equalTo(4));


    }

    private Planet planet(String id) {
        Planet planet = new Planet(id);
        planet.setType(PlanetType.TYPE_A);

        return planet;
    }
}