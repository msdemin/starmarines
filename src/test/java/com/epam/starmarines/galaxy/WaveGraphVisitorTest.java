package com.epam.starmarines.galaxy;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.testng.Assert.*;

public class WaveGraphVisitorTest {

    @Test
    public void testNextWave() throws Exception {
        // (1) --- (2) --- (3) --- (6) --- (7)
        //           \    /   \   /
        //            (4) ---- (5)
        //      (8)--/           \--- (9)


        Graph<Integer, DefaultEdge> g = new SimpleGraph<>(DefaultEdge.class);

        for (int v = 1; v < 10; v++) {
            g.addVertex(v);
        }

        g.addEdge(1, 2);
        g.addEdge(2, 3);
        g.addEdge(2, 4);
        g.addEdge(3, 6);
        g.addEdge(6, 7);
        g.addEdge(6, 5);
        g.addEdge(5, 3);
        g.addEdge(4, 5);
        g.addEdge(8, 4);
        g.addEdge(5, 9);
        g.addEdge(3, 4);

        WaveGraphVisitor<Integer> visitor = new WaveGraphVisitor<>(g, 1);

        assertThat(visitor.nextWave(), containsInAnyOrder(2));
        assertThat(visitor.nextWave(), containsInAnyOrder(3, 4));
        assertThat(visitor.nextWave(), containsInAnyOrder(5, 6, 8));
        assertThat(visitor.nextWave(), containsInAnyOrder(7, 9));
        assertThat(visitor.nextWave(), empty());

    }
}