package com.epam.starmarines.bot.logic;

import com.epam.starmarines.galaxy.Planet;
import com.epam.starmarines.galaxy.PlanetType;
import org.testng.annotations.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class UtilsTest {

    @Test
    public void testNextSize() {

        assertThat(Utils.nextSize(planet(PlanetType.TYPE_D, 100)), equalTo(130));
        assertThat(Utils.nextSize(planet(PlanetType.TYPE_D, 900)), equalTo(1000));

    }

    @Test
    public void testMaxRegeneration() throws Exception {

        assertThat(Utils.realRegen(planet(PlanetType.TYPE_A, 50)), equalTo(5));

        assertThat(Utils.realRegen(planet(PlanetType.TYPE_D, 500)), equalTo(150));
        assertThat(Utils.realRegen(planet(PlanetType.TYPE_D, 900)), equalTo(100));

        assertThat(Utils.realRegen(planet(PlanetType.TYPE_B, 199)), equalTo(1));
        assertThat(Utils.realRegen(planet(PlanetType.TYPE_B, 200)), equalTo(0));
        assertThat(Utils.realRegen(planet(PlanetType.TYPE_B, 201)), equalTo(0));

    }

    private Planet planet(PlanetType typeA, int units) {
        Planet planet = new Planet("1");
        planet.setType(typeA);
        planet.setUnits(units);
        return planet;
    }
}