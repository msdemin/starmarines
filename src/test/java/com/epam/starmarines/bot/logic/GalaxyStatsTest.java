package com.epam.starmarines.bot.logic;

import com.epam.starmarines.galaxy.Planet;
import com.epam.starmarines.galaxy.PlanetType;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;


import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.testng.Assert.*;

public class GalaxyStatsTest {

    @Test
    public void testEmpires() throws Exception {

        List<Planet> planets = Arrays.asList(
                new Planet("1", "me", 100, PlanetType.TYPE_A),
                new Planet("2", "", 200, PlanetType.TYPE_A),
                new Planet("3", "me", 400, PlanetType.TYPE_A),
                new Planet("4", "", 800, PlanetType.TYPE_A),
                new Planet("5", "bot1", 1600, PlanetType.TYPE_A),
                new Planet("6", "bot2", 3200, PlanetType.TYPE_A)
        );

        GalaxyStats stats = new GalaxyStats();

        Map<String, Empire> empires = stats.empires(planets);

        assertThat(empires, hasEntry("me", new Empire("me", 500)));
        assertThat(empires, hasEntry("bot1", new Empire("bot1", 1600)));


    }
}