package com.epam.starmarines.bot.logic;

import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Максим on 13.08.2014.
 */
public class ComparatorTest {


    @Test
    public void testCompare() {

        Comparator<Integer> c = (o1, o2) -> o1 - o2;

        List<Integer> sorted = Arrays.asList(2, 4, 5, 3, 1, 6).stream().sorted(c).collect(Collectors.toList());

        System.out.println(sorted);


    }

}
