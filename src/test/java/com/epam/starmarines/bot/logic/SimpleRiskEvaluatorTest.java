package com.epam.starmarines.bot.logic;

import com.epam.starmarines.galaxy.GalaxyFactory;
import com.epam.starmarines.galaxy.Planet;
import com.epam.starmarines.galaxy.PlanetType;
import com.epam.starmarines.galaxy.StarLane;
import org.jgrapht.Graph;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.testng.Assert.*;

public class SimpleRiskEvaluatorTest {

    @Test
    public void testEvaluate() throws Exception {

        Planet[] planets = new Planet[]{
                new Planet("0", "", 0, PlanetType.TYPE_D),
                new Planet("1", "bot1", 100, PlanetType.TYPE_C),
                new Planet("2", "bot2", 120, PlanetType.TYPE_C),
                new Planet("3", "bot2", 30, PlanetType.TYPE_C),
                new Planet("4", "bot3", 50, PlanetType.TYPE_C),
                new Planet("5", "", 0, PlanetType.TYPE_C)};

        setNeighbours(planets, 0, new int[] {1,2,3,4,5});
        setNeighbours(planets, 1, new int[] {0, 2, 5});
        setNeighbours(planets, 2, new int[] {0, 1, 3});
        setNeighbours(planets, 3, new int[] {0, 2, 4});
        setNeighbours(planets, 4, new int[] {0, 3, 5});
        setNeighbours(planets, 5, new int[] {0, 4, 1});

        GalaxyFactory galaxyFactory = new GalaxyFactory();
        Graph<Planet, StarLane<Planet>> galaxy = galaxyFactory.createGalaxy(Arrays.asList(planets));

        SimpleRiskEvaluator evaluator = new SimpleRiskEvaluator();

        List<PlanetInvasionRisk> risks = evaluator.evaluate(galaxy, planets[0]);

        assertThat(risks, contains(
                new PlanetInvasionRisk("bot2", 150),
                new PlanetInvasionRisk("bot1", 100),
                new PlanetInvasionRisk("bot3", 50)
        ));
    }

    @Test
    public void testEvaluateWithOwnedCenter() throws Exception {

        Planet[] planets = new Planet[]{
                new Planet("0", "bot1", 100, PlanetType.TYPE_D),
                new Planet("1", "bot1", 80, PlanetType.TYPE_C),
                new Planet("2", "bot2", 120, PlanetType.TYPE_C),
                new Planet("3", "bot2", 30, PlanetType.TYPE_C),
                new Planet("4", "bot3", 50, PlanetType.TYPE_C),
                new Planet("5", "", 0, PlanetType.TYPE_C)};

        setNeighbours(planets, 0, new int[] {1,2,3,4,5});
        setNeighbours(planets, 1, new int[] {0, 2, 5});
        setNeighbours(planets, 2, new int[] {0, 1, 3});
        setNeighbours(planets, 3, new int[] {0, 2, 4});
        setNeighbours(planets, 4, new int[] {0, 3, 5});
        setNeighbours(planets, 5, new int[] {0, 4, 1});

        GalaxyFactory galaxyFactory = new GalaxyFactory();
        Graph<Planet, StarLane<Planet>> galaxy = galaxyFactory.createGalaxy(Arrays.asList(planets));

        SimpleRiskEvaluator evaluator = new SimpleRiskEvaluator();

        List<PlanetInvasionRisk> risks = evaluator.evaluate(galaxy, planets[0]);

        assertThat(risks, contains(
                new PlanetInvasionRisk("bot1", 100 + 30 + 80 + 24),
                new PlanetInvasionRisk("bot2", 150),
                new PlanetInvasionRisk("bot3", 50)
        ));
    }

    private void setNeighbours(Planet[] planets, int i, int[] neighbours) {
        for (int neighbour : neighbours) {
            planets[i].addNeighbours(planets[neighbour]);
        }
    }
}