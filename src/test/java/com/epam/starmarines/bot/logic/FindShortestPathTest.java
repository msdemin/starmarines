package com.epam.starmarines.bot.logic;

import org.jgrapht.Graph;
import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import java.util.List;

/**
 * Created by Максим on 04.08.2014.
 */
public class FindShortestPathTest {

    private static final Logger log = LoggerFactory.getLogger(FindShortestPathTest.class);

    @Test
    public void testFindShortestPath() {

        // (1) --- (2) --- (3) --- (6) --- (7)
        //           \    /   \   /
        //            (4) ---- (5)
        //      (8)--/           \--- (9)


        Graph<Integer, DefaultEdge> g = new SimpleGraph<>(DefaultEdge.class);

        for (int v = 1; v < 10; v++) {
            g.addVertex(v);
        }

        g.addEdge(1, 2);
        g.addEdge(2, 3);
        g.addEdge(2, 4);
        g.addEdge(3, 6);
        g.addEdge(6, 7);
        g.addEdge(6, 5);
        g.addEdge(5, 3);
        g.addEdge(4, 5);
        g.addEdge(8, 4);
        g.addEdge(5, 9);
        g.addEdge(3, 4);

        List<DefaultEdge> pathBetween1and8 = DijkstraShortestPath.findPathBetween(g, 1, 8);
        log.info(pathBetween1and8.toString());


    }
}
